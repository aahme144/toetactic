FROM golang:1.15 as builder
WORKDIR /User/container/toetactic
ARG goos=linux
ARG goarch=amd64
ARG tag=latest
RUN export GO111MODULE=on &&\
    go get gitlab.com/aahme144/toetactic@$tag
ENV GOOS=$goos \
    GOARCH=$goarch \
    CGO_ENABLED=0
RUN cd  "$(\ls -1dt $GOPATH/pkg/mod/gitlab.com/aahme144//*/ | head -n 1)" &&\
    go build -o /User/container/toetactic/toetactic .


FROM registry:2.7.1
WORKDIR /
COPY --from=builder /User/container/toetactic/toetactic /bin/toetactic
RUN chmod +x /bin/toetactic
EXPOSE 9888

ENTRYPOINT ["toetactic"]
CMD ["-port", "8080"]