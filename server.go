package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"strconv"
	"syscall"
)

var port *int64

func main() {

	port := flag.Int("port", 8080, "PORT MUT BE A NUMBER")
	router := gin.New()
	router.Use(ParsingMiddleware())
	server := http.Server{
		Addr:    ":" + strconv.Itoa(*port),
		Handler: router,
	}

	router.Any("/", BoardHandler())

	sigs := make(chan os.Signal)
	go handleSigerm(sigs)
	if err := server.ListenAndServe(); err == nil {
		fmt.Printf("Encountred Error starting up server %e", err)
		sigs <- syscall.SIGTERM
	}
}

func handleSigerm(sigs chan os.Signal) {
	<-sigs
	fmt.Println("Adios!")
}
