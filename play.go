package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httputil"
	"strings"
)

const sizeOfABoard = 9
const boardKey = "board"
const emptyIndexKeys = "emptyindex"

// shortcut maps to special squares on the board
var PlayerToPrimeNumber = map[string]int{"X": 3, "O": 5, " ": 0}
var MiddleSquaresAtEgde = []int{1, 3, 5, 7}
var OppositeCornerMap = map[int]int{0: 8, 2: 6}
var CornerAdjacentNeighnourMap = map[int][]int{2: []int{1, 5}, 6: []int{3, 7}, 8: []int{7, 5}, 0: []int{1, 3}}

// unorder ways of winning
var winningOutcomes = [][]int{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}}

func BoardHandler() func(c *gin.Context) {

	return (func(c *gin.Context) {

		board, _ := c.Get(boardKey)
		boardValues := board.([]string)
		emptyIndexinterface, _ := c.Get(emptyIndexKeys)
		emptyIndex := emptyIndexinterface.([]int)
		ImplementBestMove(boardValues, emptyIndex)
		DisplayBoard(boardValues)
		result := strings.ToLower(strings.Join(boardValues, ""))
		c.String(http.StatusOK, "%s", result)
	})
}

func ParsingMiddleware() func(c *gin.Context) {
	return (func(c *gin.Context) {
		fmt.Println(c.Request.RequestURI)
		request, _ := httputil.DumpRequest(c.Request, true)
		fmt.Printf("body %s:", request)
		board := c.Query("board")
		if board == "" {
			fmt.Println("Error: Query values are missing")
			c.JSON(400, gin.H{"error": "Query values are missing"})
			c.AbortWithStatus(400)
			return
		}
		board = strings.ToUpper(board)
		boardValues := strings.Split(board, "")
		err, emptyIndex := SanitizeBoardValues(boardValues)
		if err != nil {
			fmt.Printf("Invalid Board , %e", err)
			c.AbortWithStatus(400)
			return
		}
		DisplayBoard(boardValues)
		c.Set(boardKey, boardValues)
		c.Set(emptyIndexKeys, emptyIndex)
		c.Next()
	})
}

func DisplayBoard(boardValues []string) {

	for i := 0; i < 3; i++ {
		fmt.Println("")
		fmt.Printf("%v", boardValues[i*3:i*3+3])
		fmt.Println("")
	}
	fmt.Println("")
}

func SanitizeBoardValues(boardValues []string) (error, []int) {
	movesCountermap := map[string]int{"X": 0, "O": 0, " ": 0}
	emptyIndex := []int{}
	if len(boardValues) != sizeOfABoard {
		return fmt.Errorf("%v is not a valid tictactoe board", boardValues), emptyIndex
	}
	for i, value := range boardValues {
		if value != "X" && value != "O" && value != " " {
			return fmt.Errorf("%s is not a valid character in tictactoe", value), emptyIndex
		}
		movesCountermap[value]++
		if value == " " {
			emptyIndex = append(emptyIndex, i)
		}
	}
	if movesCountermap[" "] == 0 {
		return fmt.Errorf("No moves to make"), emptyIndex
	}
	if movesCountermap["O"]+movesCountermap["X"]+movesCountermap[" "] != sizeOfABoard {
		return fmt.Errorf("this isn't a standard board setup"), emptyIndex
	}
	if (movesCountermap["X"] - movesCountermap["O"]) >= 2 {
		return fmt.Errorf("X played on two or more turns"), emptyIndex
	}
	if (movesCountermap["O"] - movesCountermap["X"]) >= 2 {
		return fmt.Errorf("O played on two or more turns"), emptyIndex
	}
	if (movesCountermap["O"] - movesCountermap["X"]) == 1 {
		return fmt.Errorf("Cant play not (O) our turn"), emptyIndex
	}
	// check game ended
	if IsGameOver(boardValues) {
		return fmt.Errorf("The game already has a winner"), emptyIndex
	}
	// everything checks out
	return nil, emptyIndex

}
func PlayerWon(board []string, player int) (bool, int) {

	for i := 0; i < sizeOfABoard-1; i++ {
		sum := 0
		for j := 0; j < 3; j++ {
			sum += PlayerToPrimeNumber[board[winningOutcomes[i][j]]]
		}
		if sum == 2*player {
			for j := 0; j < 3; j++ {
				if board[winningOutcomes[i][j]] == " " {
					return true, winningOutcomes[i][j]
				}
			}
		}
	}
	return false, 0
}

func ImplementBestMove(board []string, emptyIndex []int) {

	// Prioritize Stategy 5, 6 and 3 if it is our first move or our second move
	// to maximize the chances of winning
	if len(emptyIndex) >= sizeOfABoard-2 {
		// Strategy 5 Centre mark: A player marks the center
		if CentreMark(board) {
			return
		}
		// Player O must always respond to a center opening with a corner mark.
		// Any other responses will allow X to force the win
		if board[4] == "X" {
			// Strategy 6 .Opposite corner: If the opponent is in the corner, the player plays the opposite corner.
			// TODO:Strategy 3. Fork??
			if CornerMark(board) {
				return
			}
		}
	} else {
		// Priotize Strategy 1 and 2 i.e Try to win
		// immediately or block a winning chance for X

		//Strategy 1 Win
		playerCanWin, winningBoardIndex := CanPlayerWin(board, PlayerToPrimeNumber["O"])
		if playerCanWin {
			board[winningBoardIndex] = "O"
			return
		}
		//Strategy 2 Block
		playerCanWin, winningBoardIndex = CanPlayerWin(board, PlayerToPrimeNumber["X"])
		if playerCanWin {
			board[winningBoardIndex] = "O"
			return
		}

		// Prioritize Stategy 4 and 7 if the game progresses
		// while trying to bloack any suspicious oponent forks

		// Strategy 4 Blocking an opponent's fork:

		// for  suspicious fork scenario 1, X marked opposite corners
		// Use Stategy 8
		if OponentTriedOppositeCornerFork(board) && board[4] == "O" {
			if MarkAnyMiddleEdge(board) {
				return
			}
		}
		// for suspicious fork scenario 2, X marked neighour to empty corners
		// Use Stategy 7
		if ok, blockIndex := OponentTriedNeighbourCornerFork(board); ok {
			//block
			board[blockIndex] = "0"
			return
		}
	}
	// Strategy 7,8
	if CornerMark(board) || MarkAnyCorner(board) || MarkAnyMiddleEdge(board) {
		return
	}
	// otherwise any answer is the best answer
	MarkAnything(board, emptyIndex)

	return

}

func CentreMark(board []string) bool {
	if board[4] == " " {
		board[4] = "O"
		return true
	}
	return false
}

func CornerMark(board []string) bool {
	emptyCorner := []int{}
	for corner, oppositecorner := range OppositeCornerMap {
		if board[corner] == "X" && board[oppositecorner] == " " {
			board[oppositecorner] = "O"
			return true
		}
		if board[oppositecorner] == "X" && board[corner] == " " {
			board[corner] = "O"
			return true
		}
		if board[corner] == " " {
			emptyCorner = append(emptyCorner, corner)
		}
		if board[oppositecorner] == " " {
			emptyCorner = append(emptyCorner, corner)
		}
	}

	if len(emptyCorner) > 0 {
		//Prioritize Corner with no occupied neighbour
		for corner, neighbors := range CornerAdjacentNeighnourMap {
			if board[corner] == " " {
				isBothNeigboursUnMarked := true
				for _, neighbour := range neighbors {
					isBothNeigboursUnMarked = board[neighbour] == " " && isBothNeigboursUnMarked
				}
				if isBothNeigboursUnMarked {
					board[corner] = "O"
					return true
				}
			}
		}
	}
	return false
}

func OponentTriedOppositeCornerFork(board []string) bool {
	for corner, oppositecorner := range OppositeCornerMap {
		if board[corner] == "X" && board[oppositecorner] == "X" {
			return true
		}
	}
	return false
}

func OponentTriedNeighbourCornerFork(board []string) (bool, int) {
	for corner, neighbors := range CornerAdjacentNeighnourMap {
		if board[corner] == " " {
			isBothNeigboursMarkedByOpponent := true
			for _, neighbour := range neighbors {
				isBothNeigboursMarkedByOpponent = board[neighbour] == "X" && isBothNeigboursMarkedByOpponent
			}
			if isBothNeigboursMarkedByOpponent {
				return true, corner
			}
		}
	}
	return false, 0
}

func CanPlayerWin(board []string, player int) (bool, int) {

	for i := 0; i < len(winningOutcomes)-1; i++ {
		sum := 0
		for j := 0; j < 3; j++ {
			sum += PlayerToPrimeNumber[board[winningOutcomes[i][j]]]
		}
		if sum == 2*player {
			for j := 0; j < 3; j++ {
				if board[winningOutcomes[i][j]] == " " {
					return true, winningOutcomes[i][j]
				}
			}
		}
	}
	return false, 0
}

func MarkAnyCorner(board []string) bool {
	emptyCorner := []int{}
	for corner, oppositecorner := range OppositeCornerMap {
		if board[corner] == " " {
			emptyCorner = append(emptyCorner, corner)
		}
		if board[oppositecorner] == " " {
			emptyCorner = append(emptyCorner, corner)
		}
	}
	// pick An empty Corner
	board[emptyCorner[0]] = "O"
	return true
}

func MarkAnyMiddleEdge(board []string) bool {
	empytMiddleEdge := []int{}
	for _, middleEdge := range MiddleSquaresAtEgde {
		if board[middleEdge] == " " {
			empytMiddleEdge = append(empytMiddleEdge, middleEdge)
		}
	}
	if len(empytMiddleEdge) > 0 {
		board[empytMiddleEdge[0]] = "O"
		return true
	}
	return false
}

func MarkAnything(board []string, emptyIndex []int) bool {
	if len(emptyIndex) > 0 {
		board[emptyIndex[0]] = "O"
		return true
	}
	return false
}

func IsGameOver(board []string) bool {
	for player, playerNumbRep := range PlayerToPrimeNumber {
		if player == " " {
			continue
		}
		for i := 0; i < len(winningOutcomes)-1; i++ {
			sum := 0
			for j := 0; j < 3; j++ {
				sum += PlayerToPrimeNumber[board[winningOutcomes[i][j]]]
			}
			if sum == 3*playerNumbRep {
				return true
			}
		}
	}
	return false
}
